# Odyssea III - Resourcepack
Resourcepack projektu **Odyssea III**

## Upozornenie
* Tento resourcepack používajte s Odyssey Resourcepackom *https://www.curseforge.com/minecraft/texture-packs/odyssey-the-sci-fi-resource-pack/download/2491894*
* **Odyssea III RP musí byť nad Odyssey RP v Minecrafte!**

## Credits
### Texture Artists
* **0ptima** @Detrend (Vedúci projektu)
* **mordobi27** @petrbren4zscheb
* **JoubaMety** @joubamety

### Výpomoc
* Floras (už nepomáha a nemá GitLab)
* Mir

README spravil @joubamety
